

var dw = {

    request: require('request-promise'),

    api_url: '',

    user: '',

    pwd: '',

    // Sets the URL for the dokuwiki client
    setEndPoint: function(url){

      dw.api_url = url;

    },

    // Sets credentials for authentication before using the wiki
    setCredentials: function(user,pwd){

      dw.user = user;

      dw.pwd = pwd;

    },

    // Makes a HTTP request and returns a promise

    makeRequest:function(body){

      body.username = dw.user;

      body.password = dw.pwd;

      //require('request-debug')(dw.request);

      return dw.request(

          // options
          {

            uri: dw.api_url,

            method: 'POST',

            headers: [
                {
                  name: 'content-type',
                  value: 'application/json'
                }
            ],

            body: JSON.stringify(body)

          }

      )

    },

    // Searchs your dokuwiki and returns a request promise
    search: function (term){

      return dw.makeRequest({"method":"search","term":term});

    },

};

module.exports = dw;
