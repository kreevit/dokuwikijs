var should      = require('chai').should(),

    dw          = require('../index'),

    setEndPoint = dw.setEndPoint,

    setCredentials  = dw.setCredentials,

    search  = dw.search

describe('#setEndPoint', function() {
  it('sets the url where we can reach the php client', function() {
    setEndPoint('http://help.flx.bn/client.php');
    dw.api_url.should.not.equal('');
  });
});

describe('#setCredentials', function() {
  it('sets credentials for valid http requests', function() {
    setCredentials('joy','8J_A=i55rm:5,3V7zi$:gJJM');
    dw.user.should.not.equal('');
    dw.pwd.should.not.equal('');
  });
});

describe('#search', function() {
  it('searchs the wiki', (done) => {
    search('Check DokuwikiJS').then( (result) => {
      var data = JSON.parse(result);
      data[0].id.should.equal('flxbn:api:search-test-page');
    }).finally(done);
  });
});
