/**
 * @package DokuwikiJS
 *
 * A package for searching a dokuwiki instance using nodejs
 */

var dw = require('./lib/wiki');

module.exports = dw;
